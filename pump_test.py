import time
import sys
import RPi.GPIO as GPIO

PIN = 17

GPIO.setmode(GPIO.BCM)

GPIO.setup(PIN, GPIO.OUT, initial=GPIO.HIGH)

GPIO.output(PIN, GPIO.LOW)
time.sleep(60)
GPIO.output(PIN, GPIO.HIGH)

GPIO.cleanup()

