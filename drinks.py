# drinks.py
drink_list = [
	{
		"name": "Rum & Coke",
		"ingredients": {
			"rum": 50,
			"coke": 150
		}
	}, {
		"name": "Gin & Tonic",
		"ingredients": {
			"gin": 50,
			"tonic": 150
		}
	}, {
		"name": "Long Island",
		"ingredients": {
			"gin": 15,
			"rum": 15,
			"vodka": 15,
			"tequila": 15,
			"coke": 100,
			"oj": 30
		}
	}, {
		"name": "Screwdriver",
		"ingredients": {
			"vodka": 50,
			"oj": 150
		}
	}, {
		"name": "Margarita",
		"ingredients": {
			"tequila": 50,
			"mmix": 150
		}
	}, {
		"name": "Gin & Juice",
		"ingredients": {
			"gin": 100,
			"oj": 100
		}
	}, {
		"name": "Tequila Sunrise",
		"ingredients": {
			"tequila": 50,
			"oj": 150
		}
	}, {
		"name": "Blue Lagoon",
		"ingredients": {
			"blue_curacao": 30,
			"vodka": 30,
			"sprite": 240
		}
	}, {
		"name": "Cola Vieux",
		"ingredients": {
			"vieux": 35,
			"coke": 280,
                        "cokea": 300
		}
	}
]

drink_options = [
	{"name": "Gin", "value": "gin"},
	{"name": "Sprite", "value": "sprite"},
	{"name": "Blue Curacao", "value": "blue_curacao"},
	{"name": "Rum", "value": "rum"},
	{"name": "Vodka", "value": "vodka"},
	{"name": "Tequila", "value": "tequila"},
	{"name": "Tonic Water", "value": "tonic"},
	{"name": "Coke", "value": "coke"},
	{"name": "Orange Juice", "value": "oj"},
	{"name": "Margarita Mix", "value": "mmix"}
]
